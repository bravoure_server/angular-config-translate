## Angular Config Translate / Bravoure component

This Component configs in translations in angular.

### **Versions:**

1.0.2   - Fix condition to only happens when it is set to true
1.0.1   - Added version for renew caching
1.0     - Initial Stable version

---------------------
