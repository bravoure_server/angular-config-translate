# Bravoure - Config Translate

## Use

It is used to set the base to set up the Translate service in angular


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-config-translate": "1.0.2"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
