(function () {
    'use strict';

    // Translate Provider
    function configTranslate ($translateProvider, APP_CONFIG, PATH_CONFIG) {

        var i18nCache = data.i18n_version || 1;

        $translateProvider.useStaticFilesLoader({
            files: [
                {
                    prefix: PATH_CONFIG.ASSETS + 'i18n/locale-',
                    suffix: '.json?v=' + i18nCache
                }
            ]
        });

        $translateProvider.uniformLanguageTag('bcp47').determinePreferredLanguage(function () {
            var languages = '';
            var default_lang = '';

            // Get the languages from the API
            for (var i = 0; i < APP_CONFIG.length; i++) {
                if (APP_CONFIG[i].content.content_type == 'language') {
                    languages = APP_CONFIG[i].content.i18n;
                    default_lang = APP_CONFIG[i].content.language;
                }
            }

            return default_lang;
        });

        $translateProvider.useSanitizeValueStrategy(null);

        if (data.useMissingTranslationHandlerLog != null && data.useMissingTranslationHandlerLog == true) {
            $translateProvider.useMissingTranslationHandlerLog();
        }
    }

    configTranslate.$inject =['$translateProvider', 'APP_CONFIG', 'PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .config(configTranslate);

})();
